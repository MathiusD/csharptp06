﻿using System;
using Mesozoic;

namespace Ingen
{
    public class Laboratory
    {
        public static Dinosaur createDinosaur(string name, string specie)
        {
            return new Dinosaur(name, specie, 0);
        }
    }
}