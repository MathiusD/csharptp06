﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;
using Ingen;

namespace MesozoicConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Laboratory.createDinosaur("Henry", "Diplodocus");
            Laboratory.createDinosaur("Louis", "Stegausaurus");
            Console.ReadKey();
        }
    }
}
